package com.example.forthaplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    EditText vvod;
    Button ok;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        vvod = (EditText) findViewById(R.id.exemple);
        ok = (Button) findViewById(R.id.resolve);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (vvod.getText().toString().contains("+")) {
                    String[] array = vvod.getText().toString().split("\\+");
                    int result1 = Integer.parseInt(String.valueOf(array[0]));
                    int result2 = Integer.parseInt(String.valueOf(array[1]));
                    vvod.setText(String.valueOf(result1 + result2));

                } else if (vvod.getText().toString().contains("-")) {
                    String[] array = vvod.getText().toString().split("-");
                    int result1 = Integer.parseInt(String.valueOf(array[0]));
                    int result2 = Integer.parseInt(String.valueOf(array[1]));
                    vvod.setText(String.valueOf(result1 - result2));

                } else if (vvod.getText().toString().contains("*")) {
                    String[] array = vvod.getText().toString().split("\\*");
                    int result1 = Integer.parseInt(String.valueOf(array[0]));
                    int result2 = Integer.parseInt(String.valueOf(array[1]));
                    vvod.setText(String.valueOf(result1 * result2));
                    
                } else if (vvod.getText().toString().contains("/")) {
                    String[] array = vvod.getText().toString().split("/");
                    int result1 = Integer.parseInt(String.valueOf(array[0]));
                    int result2 = Integer.parseInt(String.valueOf(array[1]));
                    vvod.setText(String.valueOf(result1 / result2));
                }

            }
        });

    }
}
